Functional Overview:

This module provides a base class for controllers for managing the processing
of drupal queues in a structured way. Also utilities to process the queue
controllers on batch operation, allowing seamless processing on cron and batch
with the same code base. The three utility classes are:

 -  QueueApi - provides utility methods for managing queue controllers.
  Check the class for details.

 -  QueueApiController - this is a base class to use for defining your own
  queue controllers. To link a standard Drupal queue with a controller, in the
  hook_cron_queue_info and add the 'queue-controller' property with the value of
  your controller's class name. Add the Controller in the files[] section in
  your module's info file and you should be good to go.

 -  QueueApiControllerFactory - base class for controllers factory. If the
  default factory can not be used to create your controller's class, specify
  the factory class in the queue's definition within the property
  'queue-controller-factory'.
