<?php

/**
 * @file
 * File that contains the QueueApi utility class definition.
 */

/**
 * QueueApi utility class for handling queue controller instances.
 *
 * @author ndobromirov
 */
class QueueApi {

  /**
   * Constant to be used in queue definition.
   */
  const KEY = 'queue api';

  /**
   * Batch result key constant.
   *
   * Used for storing control data needed in the batch finish action internals.
   */
  const RESULT_KEY = '__queue_api';

  /**
   * Time buffer for the batch operation processig.
   */
  const BATCH_TIME_PADDING = 10;

  /**
   * Time to wait for a locked item per iteration.
   *
   * Note that this is REQUIRED to be smaller than BATCH_TIME_PADDING, otherwise
   * this can result in PHP failing by time limits.
   */
  const BATCH_WAIT_TIME = 5;

  /**
   * Time in seconds that the batch iteration will hold a queue item.
   *
   * This needs to be between BATCH_WAIT_TIME and BATCH_TIME_PADDING.
   */
  const BATCH_CLAIM_PERIOD = 10;

  /**
   * Common PHP default time limit for HTTP requests.
   */
  const PHP_DEFAULT_TIME_LIMIT = 30;

  /**
   * Static cache for QueueApi::get() method.
   *
   * @var array
   *   Associative array of "queue name" => instance pairs.
   */
  private static $instances = array();

  /**
   * Simple factory method.
   *
   * This will provide controller instance for a given queue name.
   *
   * @param string $name
   *   Name of queue to process on.
   *
   * @return QueueApiController
   *   Working controller instance.
   */
  public static function get($name) {
    if (!isset(self::$instances[$name])) {
      list($controller_class, $factory_class) = self::getClasses($name);
      $factory = new $factory_class($name);
      $instance = $factory->create($controller_class);
      self::$instances[$name] = $instance;
    }

    return self::$instances[$name];
  }

  /**
   * Collects the classes needed for the creation of a queue controller.
   *
   * @param string $name
   *   Name of the queue.
   *
   * @return array
   *   List of controller class name and factory class name.
   */
  private static function getClasses($name) {
    $queues = module_invoke_all('cron_queue_info');
    drupal_alter('cron_queue_info', $queues);
    if (!isset($queues[$name])) {
      throw new Exception('Queue name is not defined!');
    }

    // Extract controller class.
    $controller_class = NULL;
    if (isset($queues[$name][self::KEY]['controller'])) {
      $controller_class = $queues[$name][self::KEY]['controller'];
    }
    if (!$controller_class) {
      throw new Exception('Queue controller class not defined!');
    }

    // Exrtact factory class.
    if (isset($queues[$name][self::KEY]['queue-controller-factory'])) {
      $factory_class = $queues[$name][self::KEY]['queue-controller-factory'];
    }
    else {
      $factory_class = variable_get('queue_api_default_controller_factory', 'QueueApiControllerFactory');
    }

    return array($controller_class, $factory_class);
  }

  /**
   * Prepares and executes a batch operation on the queue.
   *
   * @param mixed $controller
   *   QueueApiController instance or string queue name to process on batch.
   * @param array $params
   *   Associative array with initial data for processing. Depends on the
   *   controller that is going to be used.
   * @param string $redirect_path
   *   Optional. Required only when this method is not used in form submit
   *   context. For details check the documentation of batch_process() and the
   *   online batch API documentation (https://www.drupal.org/node/180528).
   */
  public static function batchProcess($controller, array $params = array(), $redirect_path = NULL) {
    if (is_string($controller)) {
      $controller = self::get($controller)->init($params);
    }
    if (!($controller instanceof QueueApiController)) {
      throw new InvalidArgumentException('Invalid controller instance passed!');
    }

    $worker = array(__CLASS__, 'batchProcessWorker');
    $arguments = array(
      $controller->getQueueName(),
      (int) variable_get('queue_api_batch_process_time_limit', 20),
    );
    $batch_definition = array(
      'progress_message' => t('Processed @current out of @total items.'),
      'error_message' => t('Error encountered!'),
      'init_message' => t('Initializing.'),
      'title' => t('Processing `@queue`.', array(
        '@queue' => $controller->getTitle(),
      )),
      'finished' => '__queue_api_batch_finished',
      'operations' => array(
        array($worker, $arguments),
      ),
      // Ensure that the file is loaded.
      'file' => str_replace(DRUPAL_ROOT . '/', '', __FILE__),
    );
    batch_set($batch_definition);
    if ($redirect_path !== NULL) {
      batch_process($redirect_path);
    }
  }

  /**
   * Batch processing worker method.
   *
   * Multiple queue ites are processed on one request, until a giggen time limit
   * is reached.
   *
   * @param string $name
   *   Queue name to work on.
   * @param int $limit
   *   Time limit in seconds. Real time limit used can be lower than the value
   *   passed, based on the time left in the PHP request.
   * @param array $context
   *   Batch API context argument passed by reference.
   */
  public static function batchProcessWorker($name, $limit, array &$context) {
    // Initialization.
    self::initTimeLimit($limit);
    $controller = self::get($name);
    $queue = $controller->getQueue();
    if (!isset($context['sandbox']['progress'])) {
      $context['sandbox']['progress'] = 0;
      $context['sandbox']['max'] = $queue->numberOfItems();
      $context['results'][self::RESULT_KEY] = array(
        'queue_name' => $name,
        'errors' => array(),
      );
    }
    $max = &$context['sandbox']['max'];
    $progress = &$context['sandbox']['progress'];
    $errors = &$context['results'][self::RESULT_KEY]['errors'];

    // Process items until one of the conditions fail.
    while (self::hasTime() && $max != $progress && $queue->numberOfItems()) {
      if (($item = $queue->claimItem(self::BATCH_CLAIM_PERIOD)) !== FALSE) {
        $controller->dispatch($item->data);
        $queue->deleteItem($item);
      }
      else {
        // Wait for the item's lock to expire.
        sleep(self::BATCH_WAIT_TIME);
        $controller->releaseItems();
        watchdog('Queue API', 'Unable to claim item from @q!', array('@q' => $name), WATCHDOG_WARNING);
      }

      // Update progress information.
      $max = ++$progress + $queue->numberOfItems();
    }

    // Aggregate the errors from the iteration (if any).
    if (($iteration_errors = $controller->getErrors())) {
      $errors = array_merge($errors, $iteration_errors);
    }

    $context['message'] = t('Processing element @item out of @total.', array(
      '@item' => $progress,
      '@total' => $max,
    ));
    // Inform the batch engine that we are not finished,
    // and provide an estimation of the completion level we reached.
    if ($progress != $max) {
      $context['finished'] = $progress / $max;
    }
  }

  /**
   * Static variable used when batch processing.
   *
   * @var array
   *   Contains the batch operation time management values for the request.
   *
   * @see QueueApi::hasTime()
   * @see QueueApi::initTimeLimit($limit)
   */
  private static $timeLimits;

  /**
   * Utility predicate for the batch operation.
   *
   * @return bool
   *   When there is more time for processing in the request retruns TRUE.
   * @see QueueApi::initTimeLimit($limit)
   */
  private static function hasTime() {
    list($start, $limit) = self::$timeLimits;
    return (microtime(TRUE) - $start) < $limit;
  }

  /**
   * Helper method that calculates the execution time limit.
   *
   * Return value is based on a parameter, but only if there is enough time.
   * When the time left is less than the provided parameter, the real time left
   * is returned as the limit.
   *
   * @param int $limit
   *   Limit requested in secods.
   *
   * @see QueueApi::hasTime()
   */
  private static function initTimeLimit($limit) {
    $start = microtime(TRUE);
    $time_passed = $start - REQUEST_TIME;

    if (!($max_execution_time = ini_get('max_execution_time'))) {
      $max_execution_time = self::PHP_DEFAULT_TIME_LIMIT;
    }

    $time_limit = $max_execution_time - $time_passed - self::BATCH_TIME_PADDING;
    $result = $time_limit > $limit ? $limit : $time_limit;
    self::$timeLimits = array($start, $result);
  }

}

/**
 * Helper method that handles the batch finish callbacks.
 *
 * There are many problems in core for making this callback in a class as a
 * [static] method, a normal static PHP function is used.
 *
 * @param bool $success
 *   TRUE on success, FALSE otherwise.
 * @param array $results
 *   List of agregated batch resuts.
 * @param array $ops
 *   List of configured operations. See batch API for details, currently
 *   ignored as we have only one worker and there is no need to track it.
 */
function _queue_api_batch_finished($success, array $results, array $ops) {
  $data = $results[QueueApi::RESULT_KEY];
  $controller = QueueApi::get($data['queue_name']);
  // Clear system data.
  unset($results[QueueApi::RESULT_KEY]);
  if ($success && empty($data['errors'])) {
    $controller->batchFinishSuccess($results);
  }
  else {
    $controller->batchFinishError($data['errors']);
  }
}
