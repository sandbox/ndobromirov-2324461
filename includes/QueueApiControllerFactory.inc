<?php

/**
 * @file
 * File that contains the default factory class.
 *
 * It is used for the creation of concrete controllers for particular queue.
 */

/**
 * Simple base factory class.
 *
 * This will allow other modules to extend freely the creation and structure of
 * both the factory classes and the controller classes.
 *
 * @author ndobromirov
 */
class QueueApiControllerFactory {
  /**
   * Queue name.
   *
   * @var string
   *   Name of the queue to work with.
   */
  protected $name;

  /**
   * Factory constructor.
   *
   * @param string $name
   *   Queue name.
   */
  public function __construct($name) {
    $this->name = $name;
  }

  /**
   * Factory method implementation.
   *
   * @param string $controller_class
   *   Name of the controller class.
   *
   * @return QueueApiController
   *   Controller instance to manage the queue.
   */
  public function create($controller_class) {
    $instance = new $controller_class();
    $instance->setQueueName($this->name);
    return $instance;
  }
}
