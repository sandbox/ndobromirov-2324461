<?php

/**
 * @file
 * File that contains example implementation of a custom queue controller.
 */

/**
 * Implements simple node processor queue controller for large amounts of nodes.
 *
 * Keep in mind that the initializeQueue method needs to be fast, as on batch
 * operations it is run on an HTTP request, effectively limiting the execution
 * time.
 *
 * All the processing needs to happen in the queue action methods. Keep them as
 * fast and as small as possible, for example one operation each or operations
 * that will not go over 10 seconds, otherwise you are risking the loss of the
 * batch operations support.
 *
 * For complex queues, just implement many actions and flows as needed.
 *
 * @author ndobromirov
 */
class QueueApiExampleController extends QueueApiController {

  /**
   * Collect all node IDs and pass them in the queue for processig in chunks.
   *
   * @param array $data
   *   Assiciative array with input parameters. This depends on the user.
   *
   * @see QueueApi::init()
   */
  protected function initializeQueue(array $data) {
    // Assume that the node IDs ara so many that filling the queie 1 by one will
    // hit a time limit.
    $nids = db_select('node', 'n')
      ->fields('n', array('nid'))
      ->execute()
      ->fetchCol();

    // So split in parts of 200
    foreach (array_chunk($nids, 200) as $chunk) {
      $this->addAction('queueChunk', array('chunk' => $chunk));
    }
  }

  /**
   * Process single chunk of node IDs by adding an item for every one of them.
   *
   * @param array $data
   *   Holds the parameters passed to the action.
   */
  public function actionQueueChunk(array $data) {
    foreach ($data['chunk'] as $nid) {
      $this->addAction('processNode', array('nid' => $nid));
    }
  }

  /**
   * Process single node.
   *
   * @param array $data
   *   Holds the parameters passed to the action.
   */
  public function actionProcessNode(array $data) {
    if (($node = node_load($data['nid'])) === FALSE) {
      $message = t('Unable to load nide @nid!', array('@nid' => $data['nid']));
      return $this->failed($message);
    }

    // Some hevy calculations and changes on the node instance.
    sleep(5);
    watchdog('Queue API Example', 'Processed node @node!', array(
      '@node' => $node->nid,
    ));
  }
}
